Tarefa:

1 - Criar uma tabela de cidades no banco de dados contendo os campos id, nome, uf <- varchar (2) "Rs", "Sc"
2 - Criar uma listagem das cidades 
3 - Criar um formulário das cidades -> o campo da uf precisa ser um Select 
4 - Entregar  o CRUD (Create, Read, Update, Delete)

Como fazer a entrega:

1 - Criar branch com o que foi feito = git checkout -b "uma_descrição_sem_espaços_e_caracteres_especiais"
2 - Adicionar arquivos novos que foram criados = git add .
3 - Juntar o código (commitar) - git commit - am "descrição do que foi feito"
4 - Enviar código = git push. Se der uma mensagem de não ter repositório, copiar o retorno e colar.
5 - Fazer o merge request, copiando o link que retorna após a criação.
6 - Gravar um  vídeo (no loom ou no vidyard, demonstrando o que foi feito)

Obs: para voltar para a branch principal, utilize o git checkout main